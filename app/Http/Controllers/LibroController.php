<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;

class LibroController extends Controller
{
    public function listarLibros() {
    	$libros = Libro::all();

    	if($libros->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encontraron libros registrados'], 404);
    	}

    	return response()->json($libros, 200);
    }

    public function obtenerLibro($id) {
    	$libro = Libro::find($id);

    	if(!$libro) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	return response()->json($libro, 200);
    }

    public function agregarLibro(Request $request) {
    	$request->validate([
    		'titulo' => 'string|required',
    		'fecha_publicacion' => 'date_format:Y-m-d|required',
    		'tipo' => 'string|required',
    		'descripcion' => 'string|required',
    	]);

    	$libro = new Libro([
    		'titulo' => $request->titulo,
    		'fecha_publicacion' => $request->fecha_publicacion,
    		'tipo' => $request->tipo,
    		'descripcion' => $request->descripcion,
    	]);

    	$libro->save();

    	return response()->json(['mensaje' => 'Datos agregados con éxito'], 201);
    }

    public function actualizarDatosLibro(Request $request, $id) {
    	$request->validate([
    		'titulo' => 'string|required',
    		'fecha_publicacion' => 'date_format:Y-m-d|required',
    		'tipo' => 'string|required',
    		'descripcion' => 'string|required',
    	]);

    	$libro = Libro::find($id);

    	if(!$libro) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	$libro->titulo = $request->titulo;
    	$libro->fecha_publicacion = $request->fecha_publicacion;
    	$libro->tipo = $request->tipo;
    	$libro->descripcion = $request->descripcion;
    	$libro->save(); 

    	return response()->json(['mensaje' => 'Datos actualizados con éxito'], 200);
    }

    public function obtenerAutoresLibro($id) {
    	$libro = Libro::find($id);

    	if(!$libro) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	if($libro->autores->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encontraron libros asociados al autor especificado'], 404);
    	}

    	return response()->json($libro->autores, 200);
    }
}