<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;
use App\Libro;

class AutorController extends Controller
{
    public function listarAutores() {

    	$autores = Autor::all();

    	if($autores->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encuentran autores registrados'], 404);
    	}

    	return response()->json($autores, 200);
    }

    public function obtenerAutor($id) {

    	$autor = Autor::find($id);

    	if(!$autor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	return response()->json($autor, 200);
    }

    public function agregarAutor(Request $request) {
    	$request->validate([
    		'nombre' => 'string|required',
    		'nacionalidad' => 'string|required',
    		'bio' => 'string|required',
    	]);

    	$autor = new Autor([
    		'nombre' => $request->nombre,
    		'nacionalidad' => $request->nacionalidad,
    		'bio' => $request->bio,
    	]);

    	$autor->save();

    	return response()->json(['mensaje' => 'Datos agregados con éxito'], 201);
    }

    public function actualizarDatosAutor(Request $request, $id) {
    	$request->validate([
    		'nombre' => 'string|required',
    		'nacionalidad' => 'string|required',
    		'bio' => 'string|required',
    	]);

		$autor = Autor::find($id);

		if(!$autor) {
			return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
		}

		$autor->nombre = $request->nombre;
		$autor->nacionalidad = $request->nacionalidad;
		$autor->bio = $request->bio;

		$autor->save();

		return response()->json(['mensaje' => 'Datos actualizados con éxito']);
    }

    public function obtenerLibrosAutor($id) {
    	$autor = Autor::find($id);

    	if(!$autor) {
    		return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
    	}

    	if($autor->libros->isEmpty()) {
    		return response()->json(['mensaje' => 'No se encontraron libros asociados al autor especificado'], 404);
    	}

    	return response()->json($autor->libros, 200);
    }

    public function asignarAutoria($id, Request $request) {

    	$request->validate([
    		'libro_id' => 'integer|required|exists:libros,id',
    	]);

    	$autor = Autor::find($id);
    		
		if(!$autor) {
			return response()->json(['mensaje' => 'No se encontró el recurso solicitado'], 404);
		}

		if($autor->libros->contains($request->libro_id)) {
			return response()->json(['mensaje' => 'Los datos ingresados de libro y autor ya se encuentran asociados'], 400);
		}

		$autor->libros()->sync($request->libro_id);

		return response()->json(['mensaje' => 'Relación de autoría establecida con éxito'], 201);
    }
}