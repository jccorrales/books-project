<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('autores', 'AutorController@listarAutores');
Route::get('autores/{autor}', 'AutorController@obtenerAutor');
Route::post('autores', 'AutorController@agregarAutor');
Route::put('autores/{autor}', 'AutorController@actualizarDatosAutor');
Route::get('libros', 'LibroController@listarLibros');
Route::get('libros/{libro}', 'LibroController@obtenerLibro');
Route::post('libros', 'LibroController@agregarLibro');
Route::put('libros/{libro}', 'LibroController@actualizarDatosLibro');
Route::get('autores/{autor}/libros', 'AutorController@obtenerLibrosAutor');
Route::get('libros/{libro}/autores', 'LibroController@obtenerAutoresLibro');
Route::post('autores/{autor}/libros', 'AutorController@asignarAutoria');
