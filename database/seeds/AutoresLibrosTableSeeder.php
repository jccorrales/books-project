<?php

use Illuminate\Database\Seeder;
use App\Autor;
use App\Libro;

class AutoresLibrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$autores = Autor::all();

        Autor::all()->each(function ($autor) {
        	$autor->libros()->sync(Libro::inRandomOrder()->first()->id);
        });
    }
}